package com.plexonic.analytcs.plexonic.managers;

import com.plexonic.analytcs.plexonic.connectors.ConnectionDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthorizationManager extends ConnectionDB {

    public Integer isTruePartnerAuthorization(String partnerAuthorizationKey) throws SQLException {

        Integer partnerID = null;
        Connection isTruePartnerConection = null;
        PreparedStatement isTruePartnerStatment = null;
        String selectUserQuery = "SELECT id FROM partner WHERE authorization=? and is_active=1 ;";
        try {
            isTruePartnerConection = super.getConnection();
            isTruePartnerStatment = isTruePartnerConection.prepareCall(selectUserQuery);
            isTruePartnerStatment.setString(1, partnerAuthorizationKey);
            ResultSet result = isTruePartnerStatment.executeQuery();
            if (result.next()) {
                partnerID = result.getInt("id");
            }
        }finally {
            super.closeConnection(isTruePartnerConection, isTruePartnerStatment);
        }
        return partnerID;
    }

}
