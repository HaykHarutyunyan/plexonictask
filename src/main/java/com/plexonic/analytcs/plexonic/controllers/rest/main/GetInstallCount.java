package com.plexonic.analytcs.plexonic.controllers.rest.main;

import com.plexonic.analytcs.plexonic.managers.AuthorizationManager;
import com.plexonic.analytcs.plexonic.utils.JSONCreator;
import com.plexonic.analytcs.plexonic.utils.ResponseMessage;
import com.plexonic.analytcs.plexonic.utils.ResponseMessageKey;
import com.plexonic.analytcs.plexonic.utils.Util;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@RestController
public class GetInstallCount extends AuthorizationManager {

    private static final Logger log = LoggerFactory.getLogger(GetInstallCount.class);

    @RequestMapping(value = "/getRetention", method = RequestMethod.GET)
    public String getRetention(@RequestParam("intervalDay") int intervalDay,
                         @RequestHeader("token") String token) {
        JSONObject returnJSON = null;
        Connection getRetentionConection = null;
        PreparedStatement getRetentionStatment = null;
        String selectInstallCountQuery = "SELECT COUNT(*) as install_count,install_date AS date FROM user " +
                "WHERE owner_partner_id=? AND install_date>'"+ Util.getCurrentDateAddDays(-intervalDay)+"' GROUP BY install_date;";

        try {
            Integer partnerID = super.isTruePartnerAuthorization(token);
            if (partnerID > 0) {
                getRetentionConection = super.getConnection();
                getRetentionStatment = getRetentionConection.prepareCall(selectInstallCountQuery);
                getRetentionStatment.setInt(1, partnerID);

                ResultSet result = getRetentionStatment.executeQuery();
                JSONObject contentJSON = new JSONObject();
                JSONArray allAnalytics = new JSONArray();

                while (result.next()) {
                    JSONObject daylyAnalytics = new JSONObject();
                    daylyAnalytics.put("installCount", result.getString("install_count"));
                    daylyAnalytics.put("date", result.getString("date"));
                    allAnalytics.put(daylyAnalytics);
                }
                contentJSON.put("allAnalytics",allAnalytics);
                returnJSON = JSONCreator.createSuccessJSON(contentJSON);
            } else {
                returnJSON = JSONCreator.createErrorJSON(ResponseMessage.AUTHORIZATION_PROBLEM, ResponseMessageKey.AUTHORIZATION_PROBLEM);
            }
        } catch (SQLException e) {
            returnJSON = JSONCreator.createErrorJSON(ResponseMessage.SOMETHING_WENT_WRONG, ResponseMessageKey.SQL_EXEPTION);
            log.error(e.toString());
        } finally {
            super.closeConnection(getRetentionConection, getRetentionStatment);
        }
        return returnJSON.toString();
    }



}
