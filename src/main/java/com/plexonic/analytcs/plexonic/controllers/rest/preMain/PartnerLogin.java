package com.plexonic.analytcs.plexonic.controllers.rest.preMain;

import com.plexonic.analytcs.plexonic.beans.PartnerBean;
import com.plexonic.analytcs.plexonic.connectors.ConnectionDB;
import com.plexonic.analytcs.plexonic.utils.JSONCreator;
import com.plexonic.analytcs.plexonic.utils.PasswordGenerator;
import com.plexonic.analytcs.plexonic.utils.ResponseMessage;
import com.plexonic.analytcs.plexonic.utils.ResponseMessageKey;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@RestController
public class PartnerLogin extends ConnectionDB{

    private static final Logger log = LoggerFactory.getLogger(PartnerLogin.class);

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public JSONObject partnerLogin(
            @Valid PartnerBean partner) {

        JSONObject returnJSON = null;
        Connection partnerLoginConection = null;
        PreparedStatement partnerLoginStatment = null;
        String selectUserQuery = "SELECT authorization FROM partner WHERE (user_name=? OR mail=? OR phone=?) AND password=?";

        try {
            System.out.println(partner.getUserName());
            partnerLoginConection = super.getConnection();
            partnerLoginStatment = partnerLoginConection.prepareCall(selectUserQuery);
            partnerLoginStatment.setString(1, partner.getUserName());
            partnerLoginStatment.setString(2, partner.getUserName());
            partnerLoginStatment.setString(3, partner.getUserName());
            partnerLoginStatment.setString(4, PasswordGenerator.stringToSha256(partner.getPassword()));
            ResultSet result = partnerLoginStatment.executeQuery();

            JSONObject contentJSON = new JSONObject();
            if (result.next()) {
                contentJSON.put("accessToken", result.getString("authorization"));
            } else {
                returnJSON = JSONCreator.createErrorJSON(ResponseMessage.AUTHORIZATION_PROBLEM, ResponseMessageKey.AUTHORIZATION_PROBLEM);
                return returnJSON;
            }
            returnJSON = JSONCreator.createSuccessJSON(contentJSON);
        } catch (SQLException e) {
            returnJSON = JSONCreator.createErrorJSON(ResponseMessage.SOMETHING_WENT_WRONG, ResponseMessageKey.SQL_EXEPTION);
            log.error(e.toString());
        } finally {
            super.closeConnection(partnerLoginConection, partnerLoginStatment);
        }
        return returnJSON;
    }
}
