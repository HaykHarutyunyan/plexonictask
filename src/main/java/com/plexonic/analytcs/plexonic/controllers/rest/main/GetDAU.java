package com.plexonic.analytcs.plexonic.controllers.rest.main;

import com.plexonic.analytcs.plexonic.managers.AuthorizationManager;
import com.plexonic.analytcs.plexonic.utils.JSONCreator;
import com.plexonic.analytcs.plexonic.utils.ResponseMessage;
import com.plexonic.analytcs.plexonic.utils.ResponseMessageKey;
import com.plexonic.analytcs.plexonic.utils.Util;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@RestController
public class GetDAU extends AuthorizationManager {

    private static final Logger log = LoggerFactory.getLogger(GetDAU.class);
    /*
    DAU (Daily active users)
     */
    @RequestMapping(value = "/getDAU", method = RequestMethod.GET)
    public String getDAU(@RequestParam("intervalDay") int intervalDay,
                             @RequestHeader("token") String token) {
        JSONObject returnJSON = null;
        Connection partnerLoginConection = null;
        PreparedStatement partnerLoginStatment = null;
        String selectUserQuery = "SELECT COUNT(*) AS active_users_count,request_date AS date FROM request " +
                " INNER JOIN user ON user.`id` = `request`.`user_id` WHERE owner_partner_id=? " +
                " AND request_date>'"+ Util.getCurrentDateAddDays(-intervalDay)+"' GROUP BY request_date;";

        try {
            Integer partnerID = super.isTruePartnerAuthorization(token);
            if (partnerID > 0) {
                partnerLoginConection = super.getConnection();
                partnerLoginStatment = partnerLoginConection.prepareCall(selectUserQuery);
                partnerLoginStatment.setInt(1, partnerID);

                ResultSet result = partnerLoginStatment.executeQuery();
                JSONObject contentJSON = new JSONObject();
                JSONArray allAnalytics = new JSONArray();

                while (result.next()) {
                    JSONObject daylyAnalytics = new JSONObject();
                    daylyAnalytics.put("activeUsersCount", result.getString("active_users_count"));
                    daylyAnalytics.put("date", result.getString("date"));
                    allAnalytics.put(daylyAnalytics);
                }
                contentJSON.put("allAnalytics",allAnalytics);
                returnJSON = JSONCreator.createSuccessJSON(contentJSON);
            } else {
                returnJSON = JSONCreator.createErrorJSON(ResponseMessage.AUTHORIZATION_PROBLEM, ResponseMessageKey.AUTHORIZATION_PROBLEM);
            }
        } catch (SQLException e) {
            returnJSON = JSONCreator.createErrorJSON(ResponseMessage.SOMETHING_WENT_WRONG, ResponseMessageKey.SQL_EXEPTION);
            log.error(e.toString());
        } finally {
            super.closeConnection(partnerLoginConection, partnerLoginStatment);
        }
        return returnJSON.toString();
    }

}
