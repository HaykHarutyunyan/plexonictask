package com.plexonic.analytcs.plexonic.controllers.rest.preMain;


import com.plexonic.analytcs.plexonic.beans.PartnerBean;
import com.plexonic.analytcs.plexonic.connectors.ConnectionDB;
import com.plexonic.analytcs.plexonic.utils.JSONCreator;
import com.plexonic.analytcs.plexonic.utils.PasswordGenerator;
import com.plexonic.analytcs.plexonic.utils.ResponseMessage;
import com.plexonic.analytcs.plexonic.utils.ResponseMessageKey;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@RestController
public class ChangePasswordByConfirmCode extends ConnectionDB {

    private static final Logger log = LoggerFactory.getLogger(ChangePasswordByConfirmCode.class);

    @RequestMapping(value = "/changePasswordByConfirmCode", method = RequestMethod.POST)
    public JSONObject partnerLogin(
            @Valid PartnerBean partner){

        JSONObject returnJSON = null;
        Connection updatePartnerPasswordConection = null;
        PreparedStatement updatePartnerPasswordStatment = null;
        String updatePartnerPasswordQuery = "UPDATE partner SET password=?,confirm_code=NULL WHERE mail=? AND confirm_code=?";

        try {
            updatePartnerPasswordConection = super.getConnection();
            updatePartnerPasswordStatment = updatePartnerPasswordConection.prepareCall(updatePartnerPasswordQuery);

            updatePartnerPasswordStatment.setString(1, PasswordGenerator.stringToSha256(partner.getPassword()));
            updatePartnerPasswordStatment.setString(2, partner.getMail());
            updatePartnerPasswordStatment.setString(3, partner.getConfirmCode());
            int changedCount = updatePartnerPasswordStatment.executeUpdate();
            if (changedCount > 0) {
                returnJSON = JSONCreator.createSuccessJSON();
            }else {
                returnJSON = JSONCreator.createErrorJSON(ResponseMessage.WRONG_CONFIRM_CODE, ResponseMessageKey.WRONG_CONFIRM_CODE);
            }
        } catch (SQLException e) {
            returnJSON = JSONCreator.createErrorJSON(ResponseMessage.SOMETHING_WENT_WRONG, ResponseMessageKey.SQL_EXEPTION);
            log.error(e.toString());
        }finally{
            super.closeConnection(updatePartnerPasswordConection,updatePartnerPasswordStatment);
        }
        return returnJSON;
    }

}
