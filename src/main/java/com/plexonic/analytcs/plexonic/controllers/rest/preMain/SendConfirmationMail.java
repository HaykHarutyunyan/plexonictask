package com.plexonic.analytcs.plexonic.controllers.rest.preMain;

import com.plexonic.analytcs.plexonic.beans.PartnerBean;
import com.plexonic.analytcs.plexonic.connectors.ConnectionDB;
import com.plexonic.analytcs.plexonic.notification.SendMail;
import com.plexonic.analytcs.plexonic.utils.JSONCreator;
import com.plexonic.analytcs.plexonic.utils.ResponseMessage;
import com.plexonic.analytcs.plexonic.utils.ResponseMessageKey;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@RestController
public class SendConfirmationMail extends ConnectionDB {

    private static final Logger log = LoggerFactory.getLogger(SendConfirmationMail.class);

    @RequestMapping(value = "/sendConfirmCode", method = RequestMethod.POST)
    public JSONObject partnerLogin(@Valid PartnerBean partner){

        JSONObject returnJSON = null;
        Connection sendConfirmCodeConection = null;
        PreparedStatement sendConfirmCodeStatment = null;
        Integer confirmCode = (int)(Math.random() * 900000 + 100000);
        String updatePartnerQuery = "UPDATE partner SET confirm_code=? WHERE mail=?";

        try {
            sendConfirmCodeConection = super.getConnection();
            sendConfirmCodeStatment = sendConfirmCodeConection.prepareCall(updatePartnerQuery);

            sendConfirmCodeStatment.setString(1, confirmCode.toString());
            sendConfirmCodeStatment.setString(2, partner.getMail());
            int changedCount = sendConfirmCodeStatment.executeUpdate();
            if (changedCount > 0) {
                boolean isMailSend = SendMail.sendConfirmationMail(partner.getMail(), confirmCode.toString());
                if (isMailSend) {
                    returnJSON = JSONCreator.createSuccessJSON();
                }else {
                    returnJSON = JSONCreator.createErrorJSON(ResponseMessage.SOMETHING_WENT_WRONG, ResponseMessageKey.SEND_MAIL_EXEPTION);
                }
            }else {
                returnJSON = JSONCreator.createErrorJSON(ResponseMessage.WRONG_MAIL, ResponseMessageKey.WRONG_MAIL);
            }
        } catch (SQLException e) {
            returnJSON = JSONCreator.createErrorJSON(ResponseMessage.SOMETHING_WENT_WRONG, ResponseMessageKey.SQL_EXEPTION);
            log.error(e.toString());
        }finally{
            super.closeConnection(sendConfirmCodeConection,sendConfirmCodeStatment);
        }
        return returnJSON;
    }

}
