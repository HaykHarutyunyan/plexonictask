package com.plexonic.analytcs.plexonic.notification;

import com.plexonic.analytcs.plexonic.utils.ReadFile;
import com.plexonic.analytcs.plexonic.utils.Util;

public class MailGenerator {

    private static final String CONFIRMATION_CODE = "confirmationCode";
    private static final String USER_EMAIL_ADDRESS = "userEmailAddress";
    private static final String REPORT_DATE = "dd-mm-yyyy";
    private static final String DAU = "DAU";
    private static final String DIC = "DIC";

    protected static StringBuilder generateReportMail(final String filePath,final String reportDate,
                                                   final String dau,final String dic) {
        StringBuilder mail = new StringBuilder(ReadFile.readFile(filePath));
        Util.replaceAll(mail, REPORT_DATE,reportDate);
        Util.replaceAll(mail, DAU,dau);
        Util.replaceAll(mail, DIC,dic);
        return mail;
    }

    protected static StringBuilder generateConfirmationMail(final String filePath,final String confirmationCode,
    final String userEmailAdress) {

        StringBuilder mail = new StringBuilder(ReadFile.readFile(filePath));
        Util.replaceAll(mail, CONFIRMATION_CODE,confirmationCode);
        Util.replaceAll(mail, USER_EMAIL_ADDRESS,userEmailAdress);
        return mail;
    }
}
