package com.plexonic.analytcs.plexonic.notification;

import com.plexonic.analytcs.plexonic.beans.ReportBean;
import com.sun.mail.smtp.SMTPTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

public class SendMail extends MailGenerator {

    private static final String SMTP_YANDEX = "smtp.yandex.com";
    private static final String PROTOCOL = "smtps";
    private static final String SENDER_MAIL = "plexonic@primecard.am";
    private static final String SENDER_PASSWORD = "Qw123456789777";
    private static final String FROM = "Plexonic";
    private static final String CONFIRMATION_MAILTXT_FILE_PATH = "/var/www/files/confirmationMail.txt";
    private static final String REPORT_MAILTXT_FILE_PATH = "/var/www/files/reportMail.txt";
    private static final String EMAIL_CONFIRMATION = "E-mail Confirmation";
    private static final String DAILY_REPORT = "Daily ReportBean";


    private static final Logger log = LoggerFactory.getLogger(SendMail.class);

    public static boolean sendConfirmationMail(final String recipientEmail, final String confirmCode) {

        boolean isSend = false;
        try {
            String confirmationMail = generateConfirmationMail(CONFIRMATION_MAILTXT_FILE_PATH,confirmCode,recipientEmail).toString();
            send(recipientEmail, "",EMAIL_CONFIRMATION, confirmationMail);
            isSend = true;
        } catch (Exception exeption) {
            log.error("send Mail Error: " + exeption);
        } finally {
            return isSend;
        }
    }

    public static boolean sendReportMail(final ReportBean report) {

        boolean isSend = false;
        try {
            String reportMail = generateReportMail(REPORT_MAILTXT_FILE_PATH,report.getReportDate(),report.getDau(),report.getDic()).toString();
            send(report.getRecipientEmail(), "",DAILY_REPORT, reportMail);
            isSend = true;
        } catch (Exception exeption) {
            log.error("send Report Mail Error: " + exeption);
        } finally {
            return isSend;
        }
    }


    private static void send(String recipientEmail, String ccEmail, String title, String message) throws AddressException, MessagingException {
        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

        Properties props = System.getProperties();
        props.setProperty("mail.smtps.host", "smtp.yandex.com");
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.socketFactory.port", "465");
        props.setProperty("mail.smtps.auth", "true");


        props.put("mail.smtps.quitwait", "false");

        Session session = Session.getInstance(props, null);

        // -- Create a new message --
        final MimeMessage msg = new MimeMessage(session);

        // -- Set the FROM and TO fields --
        try {
            msg.setFrom(new InternetAddress(SENDER_MAIL ,FROM));
        } catch (UnsupportedEncodingException e) {
            log.error(e.toString());
        }
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail, false));

        if (ccEmail.length() > 0) {
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
        }

        msg.setSubject(title);
        msg.setContent(message, "text/html; charset=utf-8");
        msg.setSentDate(new Date());

        SMTPTransport t = (SMTPTransport)session.getTransport(PROTOCOL);

        t.connect(SMTP_YANDEX, SENDER_MAIL, SENDER_PASSWORD);
        t.sendMessage(msg, msg.getAllRecipients());
        t.close();
    }
}