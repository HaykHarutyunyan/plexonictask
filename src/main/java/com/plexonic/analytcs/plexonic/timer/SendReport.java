package com.plexonic.analytcs.plexonic.timer;


import com.plexonic.analytcs.plexonic.beans.ReportBean;
import com.plexonic.analytcs.plexonic.connectors.ConnectionDB;
import com.plexonic.analytcs.plexonic.notification.SendMail;
import com.plexonic.analytcs.plexonic.utils.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Component
public class SendReport extends ConnectionDB {

    private static final Logger log = LoggerFactory.getLogger(SendReport.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    private static final boolean reporter = false;

    @Scheduled(cron = "0 1 1 * * ?") //every day
    //@Scheduled(fixedRate = 300000)//every 5 minutes
    public void reportCurrentTime() {
        if(reporter) {
            log.info("The time is now {}", dateFormat.format(new Date()));
            ArrayList<ReportBean> reports = getAllPartnersDailyAnalytics();
            if (reports != null) {
                for (int i = 0; i < reports.size(); i++) {
                    SendMail.sendReportMail(reports.get(i));
                }
            }
            log.info("END");
        }

    }


    public ArrayList<ReportBean> getAllPartnersDailyAnalytics() {

        ArrayList<ReportBean> reports = null;
        Connection analyticsConection = null;
        PreparedStatement DICStatment = null;
        PreparedStatement DAUStatment = null;
        String yesterday = Util.getCurrentDateAddDays(-135);
        System.out.println(yesterday);
        String selectDICQuery = "SELECT COUNT(*) as install_count,mail,owner_partner_id AS partner_id FROM user " +
                " INNER JOIN partner ON partner.`id` = `user`.`owner_partner_id` " +
                " where DATE_FORMAT(install_date, '%Y-%m-%d') = '"+yesterday+"' GROUP BY owner_partner_id;";
        String selectDAUQuery = "SELECT COUNT(*) AS active_users_count,mail,partner.id FROM request " +
                "INNER JOIN user ON user.`id` = `request`.`user_id` " +
                "INNER JOIN partner ON partner.`id` = `user`.`owner_partner_id` " +
                "WHERE DATE_FORMAT(request_date, '%Y-%m-%d') = '"+yesterday+"' GROUP BY partner.id;";
        try {
            analyticsConection = super.getConnection();
            DICStatment = analyticsConection.prepareCall(selectDICQuery);
            DAUStatment = analyticsConection.prepareCall(selectDAUQuery);

            ResultSet DICResult = DICStatment.executeQuery();
            ResultSet DAUResult = DAUStatment.executeQuery();
            reports = new ArrayList<ReportBean>();
            int reportNumber = 0;
            while (DICResult.next() && DAUResult.next()) {
                ReportBean report = new ReportBean();
                report.setRecipientEmail(DICResult.getString("mail"));
                report.setDic(DICResult.getString("install_count"));
                report.setDau(DAUResult.getString("active_users_count"));
                report.setReportDate(yesterday);
                reports.add(reportNumber, report);
                reportNumber++;
            }
        } catch (SQLException e) {
            log.error(e.toString());
        } finally {
            super.closeConnection(analyticsConection, DICStatment, DAUStatment);
        }
        return reports;
    }


}
