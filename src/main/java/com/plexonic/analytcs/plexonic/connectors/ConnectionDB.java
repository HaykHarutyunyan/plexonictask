package com.plexonic.analytcs.plexonic.connectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ConnectionDB {

    private static final Logger log = LoggerFactory.getLogger(ConnectionDB.class);

    public Connection getConnection(){
        final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        final String DB_URL = "jdbc:mysql://127.0.0.1:3306/plexonic";
        final String USER = "root";
        final String PASS = "Qw1234567897";
        Connection myConnection = null;

        try{
            Class.forName(JDBC_DRIVER);
            myConnection = DriverManager.getConnection(DB_URL,USER,PASS);
        }catch(Exception e){
            log.error("Problem connecting to database Exception: " + e);
        }
        return myConnection;
    }

    public void closeConnection(Connection connection,PreparedStatement... statements){

        try {
            if(statements != null){
                for(int i=0;i<statements.length;i++){
                    if(statements[i] != null){
                        statements[i].close();
                    }
                }
            }
            if(connection != null){
                connection.close();
            }
        } catch (SQLException e) {
            log.error("Problem closing Statement or Connection: " + e.toString());
            e.printStackTrace();
        }
    }

}
