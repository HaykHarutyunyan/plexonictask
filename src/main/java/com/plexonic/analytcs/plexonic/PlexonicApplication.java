package com.plexonic.analytcs.plexonic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class PlexonicApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(PlexonicApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(PlexonicApplication.class);
	}
}
