package com.plexonic.analytcs.plexonic.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Util {


    public static StringBuilder replaceAll(StringBuilder builder, String from, String to)
    {
        int index = builder.indexOf(from);
        while (index != -1)
        {
            builder.replace(index, index + from.length(), to);
            index += to.length(); // Move to the end of the replacement
            index = builder.indexOf(from, index);
        }
        return builder;
    }

    public static String getCurrentDateAddDays(int dayCount) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, dayCount);
        String output = sdf.format(c.getTime());
        return output;
    }

}
