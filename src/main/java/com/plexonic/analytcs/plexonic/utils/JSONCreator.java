package com.plexonic.analytcs.plexonic.utils;

import org.json.simple.JSONObject;

public class JSONCreator {

    private static final String MESSAGE = "message";
    private static final String ERROR = "error";
    private static final String MESSAGE_KEY = "message_key";
    private static final String CONTENT = "content";

    /**
     * @author hharutyu
     * @param content
     * @return
     */
    @SuppressWarnings("unchecked")
    public static JSONObject createSuccessJSON (JSONObject content){
        JSONObject returnJSON = new JSONObject();
        //JSONObject error = new JSONObject();
        returnJSON.put(CONTENT, content);
        //returnJSON.put(ERROR, error);
        return returnJSON;
    }


    /**
     * @author hharutyu
     * @return
     */
    @SuppressWarnings("unchecked")
    public static JSONObject createSuccessJSON (){
        JSONObject returnJSON = new JSONObject();
        JSONObject content = new JSONObject();
        returnJSON.put(CONTENT, content);
        return returnJSON;
    }


    /**
     * @author hharutyu
     * @param message
     * @return
     */
    public static JSONObject createErrorJSON (ResponseMessage message, ResponseMessageKey messageKey){
        JSONObject returnJSON = new JSONObject();
        //JSONObject result = new JSONObject();
        JSONObject error = new JSONObject();
        error.put(MESSAGE_KEY, messageKey.toString());
        error.put(MESSAGE, message.toString());
        //returnJSON.put(RESULT, result);
        returnJSON.put(ERROR, error);
        return returnJSON;
    }


}
