package com.plexonic.analytcs.plexonic.utils;

public enum ResponseMessageKey {

    SUCCESS {
        public String toString() {
            return "Success";
        }
    },
    SQL_EXEPTION {
        public String toString() {
            return "SQL exeption";
        }
    },
    AUTHORIZATION_PROBLEM {
        public String toString() {
            return "Wrong userName or password";
        }
    },
    AUTHORIZATION_FAILED {
        public String toString() { return "Authorization failed"; }
    },
    SEND_MAIL_EXEPTION {
        public String toString() {
            return "Send mail Exeption";
        }
    },
    WRONG_CONFIRM_CODE {
        public String toString() {
            return "Wrong Confirm Code";
        }
    },
    WRONG_MAIL {
        public String toString() {
            return "Wrong mail";
        }
    }

}
