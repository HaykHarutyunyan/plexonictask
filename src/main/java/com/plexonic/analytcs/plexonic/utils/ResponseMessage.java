package com.plexonic.analytcs.plexonic.utils;

public enum ResponseMessage {

    SUCCESS {
        public String toString() {
            return "Success";
        }
    },
    SOMETHING_WENT_WRONG {
        public String toString() {
            return "Something went wrong\nplease try again later";
        }
    },
    AUTHORIZATION_PROBLEM {
        public String toString() {
            return "Wrong username or password";
        }
    },
    SESSION_EXPIRED {
        public String toString() {
            return "Session expired";
        }
    },
    WRONG_CONFIRM_CODE {
        public String toString() {
            return "Wrong Confirmation Code";
        }
    },
    WRONG_MAIL {
        public String toString() {
            return "Wrong E-mail address";
        }
    }

}
